from flask import Flask, jsonify, request
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open("coe332.json", "r"))

@app.route("/")
def coe332():
    return jsonify(data)


#INSTRUCTORS 
@app.route("/instructors")
def get_instructors(): 
	return jsonify(data["instructors"])

@app.route("/instructors/<int:id>")
def get_instructor_by_id(id):
	
	if len(data["instructors"]) >  id:
		return jsonify(data["instructors"][id])
	else:
		return "404 NOT FOUND"

@app.route("/instructors/<int:id>/<string:field>")
def get_instructor_name(id, field): 
	return jsonify(data["instructors"][id][field])


#MEETINGS 
@app.route("/meeting")
def get_meeting():
	return jsonify(data["meeting"])

@app.route("/meeting/<string:field>")
def get_meeting_day(field):
	return jsonify(data["meeting"][field])


#ASSIGNMENTS 

@app.route("/assignments/<int:id>")
def get_assignment_id(id): 
	return jsonify(data["assignments"][id])		

@app.route("/assignments/<int:id>/url")
def get_assignment_url(id): 
	return jsonify(data["assignments"][id]["url"])
	

@app.route("/assignments") 
def get_assignment(): 		 
	return jsonify(data["assignments"])

@app.route("/assignments", methods=['POST'])
def post_assignment():
	request_data = request.data.decode("utf-8")
	request_dictionary = (json.loads(request_data))
	data['assignments'].append(request_dictionary)
	return '200'


